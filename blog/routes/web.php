<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', 'TestController@index');
Route::get('slack', 'TestController@training');

Route::get('show', 'TestController@show');
Route::get('upload', 'TestController@showForm');

Route::post('upload', 'TestController@upload')->name('upload');

//events
Route::get('upload', 'TestController@upload')->name('upload');

Route::get('abc', 'EventController@show');