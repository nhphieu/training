<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\People;

class TestController extends Controller
{
    public function index()
    {
    	return view('test');
    }

    public function training()
    {
    	return view('slack');
    }

    public function show()
    {
    	$data = People::all();
    	return response()->json([
    		'status' => 200,
    		'message' => 'success',
    		'data' => $data
    	]);
    }

    public function showDetail($id)
    {
    	$data = People::find($id);
    	return response()->json([
    		'status' => 200,
    		'message' => 'success',
    		'data' => $data
    	]);
    }

    public function upload(Request $request)
    {
        $data = People::create($request->all());
        return response()->json($data,201);
    }

    public function delete(Request $request, $id)
    {
        $data = People::findOrFail($id);
        $tmp = People::all();
        $data->delete();
        return response()->json($tmp);
    }
}
