<!DOCTYPE html>
<html>
<head>
	<title>Slack</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/common.css">
</head>
<body>

{{-- menu --}}
<div class="row">
	@include('layout.nav_top')
</div>
<hr>
{{-- content 1 --}}
<div class="row">
	<div class="col-sm-6">
		<img src="/image/001.png" width="500px" style="padding-top: 30px;padding-left: 30px">
	</div>
	<div class="col-sm-6">
		<strong><p style="font-size: 70px">Where Work Happens</p></strong>
		<p class="content-top">When your team needs to kick off a project, hire a new employee, deploy some code, review a sales contract, finalize next year's budget, measure an A/B test, plan your next office opening, and more, Slack has you covered.</p>
		<button class="btn btn-primary content-btn">Get Start</button>
		<p>Already using slack? <a href="https://slack.com/">Sign in</a></p>
	</div>
	<hr>
</div>

{{-- content 2 --}}
<div class="row content2">
	<div class="col-sm-12">
		<strong><p class="content2-title">You’re in good company</p></strong>
		<p class="content-text">Millions of people around the world have already made Slack the place where their work happens.</p>
		<button class="btn content2-btn center-block">DISCOVER WHY</button>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4">
			<img src="/image/a001.png" style="padding: 20px;margin-left: 20px;">
		</div>
		<div class="col-sm-4">
			<img src="/image/a002.png" style="padding: 20px">
		</div>
		<div class="col-sm-4">
			<img src="/image/a003.png" style="padding: 20px">
		</div>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4">
			<img src="/image/a004.png" style="padding: 20px;margin-left: 20px">
		</div>
		<div class="col-sm-4">
			<img src="/image/a005.png" style="padding: 20px">
		</div>
		<div class="col-sm-4">
			<img src="/image/a006.png" style="padding: 20px">
		</div>
	</div>
</div>

{{-- content 3 --}}
<div class="row">
</div>

</body>
</html>